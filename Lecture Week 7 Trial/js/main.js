"use strict";

const vertexShaderSource = `
    attribute vec3 a_position;
    
    uniform mat4 u_world;
    uniform mat4 u_camera; 
    uniform mat4 u_projection;
    
    void main () {
        gl_Position = u_projection * u_camera * u_world * vec4(a_position, 1,0);
    }
`;

const fragmentShaderSource = `
    precision highp float;
    
    uniform vec3 u_colour;
    
    void main () {
        gl_FragColor = vec4(u_colour, 1.0);
    }    
`;

function createShader(gl, type, source) {
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        console.error(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}

function createProgram(gl, vertexShader, fragmentShader) {
    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    const success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        console.error(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
        return null;
    }
    return program;
}

function resize(canvas) {
    const resolution = window.devicePixelRatio || 1.0;

    const displayWidth = Math.floor(canvas.clientWidth * resolution);
    const displayHeight = Math.floor(canvas.clientHeight * resolution);

    if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        return true;
    }
    else {
        return false;
    }
}

function main() {
    //setup
    const canvas = document.getElementById("_canvas_");
    const gl = canvas.getContext("webgl");
    if( gl === null) {
        window.alert("WebGL not supported!");
        return;
    }

    // Compile the shaders
    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
    const program =  createProgram(gl, vertexShader, fragmentShader);
    gl.useProgram(program);

    // Initialise the shader attributes & uniforms
    const shader = {
        program: program
    };

    const positionAttribute = gl.getAttribLocation(program, "a_posiiton");
    const worldUniform = gl.getUniformLocation(program, "u_world");
    const cameraUniform = gl.getUniformLocation(program, "u_camera");
    const projectionUniform = gl.getUniformLocation(program, "u_projection");
    const colourUniform = gl.getUniformLocation(program, "u_colour");

    gl.enableVertexAttribArray(positionAttribute);

    const squarePositions = new Float32Array([
        -1, 0, -1,
        -1, 0, 1,

        -1, 0, 1,
        1, 0, 1,

        1, 0, 1,
        1, 0, -1,

        1, 0, -1,
        -1, 0, -1
    ]);

    const squareBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, squareBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, squareBuffer, gl.STATIC_DRAW);



    //Square
    {
        const world = glMatrix.mat4.create();
        gl.uniformMatrix4fv(worldUniform, false, world);

        gl.uniform3fv(colourUniform, [1, 0, 1]);

        gl.bindBuffer(gl.ARRAY_BUFFER, squareBuffer);
        gl.vertexAttribPointer(positionAttribute, 3, gl.FLOAT, false, 0,0 );
        gl.drawArrays(gl.LINES, 0, squarePositions.length / 3);
    }

    //Square2
    {
        const world = glMatrix.mat4.create();
        //moving it
        glMatrix.mat4.fromTranslation(world, [0, 0, 2]);
        gl.uniformMatrix4fv(worldUniform, false, world);

        gl.uniform3fv(colourUniform, [1, 0, 0]);

        gl.bindBuffer(gl.ARRAY_BUFFER, squareBuffer);
        gl.vertexAttribPointer(positionAttribute, 3, gl.FLOAT, false, 0,0 );
        gl.drawArrays(gl.LINES, 0, squarePositions.length / 3);
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0, 0,0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);

}
