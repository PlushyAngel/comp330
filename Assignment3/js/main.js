"use strict";

const vertexShaderSource = `
attribute vec4 a_position;
attribute vec2 a_texcoord;

uniform mat4 u_worldMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;

varying vec2 v_texcoord;

void main() {
    v_texcoord = a_texcoord;
    gl_Position = u_projectionMatrix * u_viewMatrix * u_worldMatrix * a_position;
}
`;

const fragmentShaderSource = `
precision mediump float;

uniform sampler2D u_texture;

varying vec2 v_texcoord;

void main() {
    gl_FragColor = texture2D(u_texture, v_texcoord); 
}
`;

//variables used to store the heightmap from JSON file
let ground = [];
let groundWidth = 0.0;
let groundDepth = 0.0;
let groundPlane = [];
let fileCheck = false;

function run(level) {
    groundWidth = level.heightmap.width;
    groundDepth = level.heightmap.depth;

    //nested for loop to push the height into array from left to right and starting from top and going to bottom
    for (let z = 0; z < groundDepth; z++) {
        for (let x = 0; x < groundWidth; x++) {
            ground.push(level.heightmap.height[x + z * groundWidth]);
        }
    }

    //check whether file was loaded
    fileCheck = true;
}    

/**
 * Load the selected level file and run it.
 * @param {*} e 
 */
function onFileSelect(e) {
    let files = e.target.files;

    if (files.length > 0) {
        let reader = new FileReader();

        reader.onload = function(e) {
            let level = JSON.parse(e.target.result);
            run(level)
        };
        reader.readAsText(files[0]);
    }
}


function createShader(gl, type, source) {
    check(isContext(gl), isString(source));

    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        console.error(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}

function createProgram(gl, vertexShader, fragmentShader) {
    check(isContext(gl), isShader(vertexShader, fragmentShader));

    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    const success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        console.error(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
        return null;
    }
    return program;
}

function main() {
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('files').addEventListener('change', onFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
        return;
    }

    // turn on antialiasing
    const contextParameters =  { antialias: true };

    // get the canvas element & gl rendering
    const canvas = document.getElementById("c");
    const gl = canvas.getContext("webgl", contextParameters);

    if (gl === null) {
        window.alert("WebGL not supported!");
        return;
    }

    //Enable depth testing and back-face culling
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.BACK);

    // Compile the shaders
    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
    const program =  createProgram(gl, vertexShader, fragmentShader);
    gl.useProgram(program);

    // Initialise the shader attributes & uniforms
    const shader = {
        program: program
    };

    const nAttributes = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    for (let i = 0; i < nAttributes; i++) {
        const name = gl.getActiveAttrib(program, i).name;
        shader[name] = gl.getAttribLocation(program, name);
        gl.enableVertexAttribArray(shader[name]);
    }

    const nUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
    for (let i = 0; i < nUniforms; i++) {
        const name = gl.getActiveUniform(program, i).name;
        shader[name] = gl.getUniformLocation(program, name);
    }

    // animation loop
    let oldTime = 0;
    let animate = function(time) {
        check(isNumber(time));

        time = time / 1000;
        let deltaTime = time - oldTime;
        oldTime = time;

        update(deltaTime);
        render();

        requestAnimationFrame(animate);
    }

    const cameraRotation = [0,0,0];
    const cameraRotationSpeed = 2 * Math.PI / 10; // radians per second
    let cameraDistance = 2;
    const cameraZoomSpeed = 1; // distance per second

    let plane;

    // update objects in the scene
    let update = function(deltaTime) {
        check(isNumber(deltaTime));

        //"player" movement.
        if (inputManager.keyPressed["ArrowLeft"]) {
            cameraRotation[1] -= cameraRotationSpeed * deltaTime;
        }
        if (inputManager.keyPressed["ArrowRight"]) {
            cameraRotation[1] += cameraRotationSpeed * deltaTime;
        }
        if (inputManager.keyPressed["ArrowUp"]) {
            cameraDistance -= cameraZoomSpeed * deltaTime;
        }
        if (inputManager.keyPressed["ArrowDown"]) {
            cameraDistance += cameraZoomSpeed * deltaTime;
        }

        //rotate camera up/down with pageup/down
        if (inputManager.keyPressed["PageUp"]) {
            cameraRotation[0] -= cameraRotationSpeed * deltaTime;
        }
        if (inputManager.keyPressed["PageDown"]) {
            cameraRotation[0] += cameraRotationSpeed * deltaTime;
        }

        //scrolling with scroll wheel
        if(inputManager.wheelDelta < 0){
            cameraDistance -= cameraZoomSpeed * deltaTime;
            inputManager.clear();
        }
        if(inputManager.wheelDelta > 0){
            cameraDistance += cameraZoomSpeed * deltaTime;
            inputManager.clear();
        }


        if(ground.length>=1 && fileCheck === true){
            plane = new Plane(gl, 20);
            plane.scale = [2,0.2,2];
            fileCheck = false;
        }
    };

    // create the matrices once, to avoid garbage collection
    // allocate matrices
    const projectionMatrix = glMatrix.mat4.create();
    const viewMatrix = glMatrix.mat4.create();
    const worldMatrix = glMatrix.mat4.create();
    const cameraPosition = glMatrix.vec3.create();

    let render = function() {
        // clear the screen
        gl.viewport(0, 0, canvas.width, canvas.height);
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        // calculate the projection matrix
        {
            const aspect = canvas.width / canvas.height;
            const fovy = glMatrix.glMatrix.toRadian(60);
            const near = 0.1;
            const far = 10;

            glMatrix.mat4.perspective(projectionMatrix, fovy, aspect, near, far);
            gl.uniformMatrix4fv(shader["u_projectionMatrix"], false, projectionMatrix);
        }

        //check if there exists a terrain
        if(ground.length>=1) {
            // set up view matrix and camera position
            {
                glMatrix.vec3.set(cameraPosition, 0, 0, cameraDistance);
                glMatrix.vec3.rotateZ(cameraPosition, cameraPosition, [0,0,0], cameraRotation[2]);
                glMatrix.vec3.rotateX(cameraPosition, cameraPosition, [0,0,0], cameraRotation[0]);
                glMatrix.vec3.rotateY(cameraPosition, cameraPosition, [0,0,0], cameraRotation[1]);
                gl.uniform3fv(shader["u_cameraPosition"], cameraPosition);

                const target = [0,0,0];
                const up = [0,1,0];
                glMatrix.mat4.lookAt(viewMatrix, cameraPosition, target, up);
                gl.uniformMatrix4fv(shader["u_viewMatrix"], false, viewMatrix);
            }

            // render the plane
            plane.render(gl, shader);
        }
    }

    // start it going
    animate(0);

}
