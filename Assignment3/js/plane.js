"use strict";

//Plane code implemented from Week 10 code.
class Plane {

    constructor(gl, nSlices) {
        this.position = [0,0,0];
        this.rotation = [0,0,0];
        this.scale = [1,1,1];
        this.matrix = glMatrix.mat4.create();

        let uvs = [];
        //calculating and pushing the uvs and points for the ground
        for (let z = 0; z < groundDepth-1; z++) {
            for (let x = 0; x < groundWidth-1; x++) {
                let xPoint1 = (x / (groundWidth - 1)) - 0.5;
                let zPoint1 = (z / (groundDepth - 1)) - 0.5;
                let xPoint2 = ((x + 1) / (groundWidth - 1)) - 0.5;
                let zPoint2 = ((z + 1) / (groundDepth - 1)) - 0.5;
                let i = (z * groundWidth) + x;

                //location of point *
                // *-.
                // |/|
                // . .
                groundPlane.push(xPoint1, ground[i], zPoint1);

                //location of point *
                // .-.
                // |/|
                // * .
                groundPlane.push(xPoint1, ground[i+20], zPoint2);

                //location of point *
                // .-*
                // |/|
                // . .
                groundPlane.push(xPoint2, ground[i+1], zPoint1);

                //^Triangle1-------------------------------------------\/Triangle2

                //location of point *
                // .-*
                // |/|
                // . .
                groundPlane.push(xPoint2, ground[i+1], zPoint1);


                //location of point *
                // .-.
                // |/|
                // * .

                groundPlane.push(xPoint1, ground[i+20], zPoint2);

                //location of point *
                // .-.
                // |/|
                // . *
                groundPlane.push(xPoint2, ground[i+21], zPoint2);

                //coordinates for the UV
                const u0 = 10* x / groundWidth;
                const u1 = 10 * (x+1) / groundWidth;

                uvs.push(u0, 0);
                uvs.push(u0, 1);
                uvs.push(u1, 0);

                uvs.push(u1, 0);
                uvs.push(u0, 1);
                uvs.push(u1, 1);
            }
        }

        this.nPoints = groundPlane.length / 3;
        this.positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(groundPlane), gl.STATIC_DRAW);

        this.uvBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uvs), gl.STATIC_DRAW);
    }

    render(gl, shader) {
        // set the world matrix
        glMatrix.mat4.identity(this.matrix);
        glMatrix.mat4.translate(this.matrix, this.matrix, this.position);
        glMatrix.mat4.rotateY(this.matrix, this.matrix, this.rotation[1]);  // heading
        glMatrix.mat4.rotateX(this.matrix, this.matrix, this.rotation[0]);  // pitch
        glMatrix.mat4.rotateZ(this.matrix, this.matrix, this.rotation[2]);  // roll
        glMatrix.mat4.scale(this.matrix, this.matrix, this.scale);
        gl.uniformMatrix4fv(shader["u_worldMatrix"], false, this.matrix);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);
        gl.vertexAttribPointer(shader["a_position"], 3, gl.FLOAT, false, 0, 0);

        //texturing stuffs
        gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
        gl.vertexAttribPointer(shader["a_texcoord"], 2, gl.FLOAT, false, 0, 0);

        gl.drawArrays(gl.TRIANGLES, 0, this.nPoints);

        //load the grass texture
        const cloverTexture = new Texture(gl, "textures/clover.png");
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, cloverTexture.texture);
        gl.uniform1i(shader["u_texture"], 0);
    }
}