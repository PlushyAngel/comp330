//Author: Keith Huynh
//Student ID: 44646216

"use strict";

class HeliRotors extends GameObject {

    //initialisation
    constructor() {
        super();

        this.position = [0, 0];
        this.rotation = Math.PI;
    }


    //draw helicopter
    renderSelf(gl, colourUniform) {
        check(isContext(gl), isUniformLocation(colourUniform));

        //set the uniforms
        let matrix = Matrix.trs(
            this.position[0], this.position[1],
            this.rotation, 1, 1);

        //set colour brown
        gl.uniform4fv(colourUniform, [1, 0, 0, 1]);

        //draw helicopter rotors
        let helicopterRotor = new Float32Array([
            0, -0.1, 0, 0.1, 0.8, 0.1,
            0.8, 0.1, 0, -0.1, 0.8, -0.1,
            0, -0.1, 0, 0.1, -0.8, 0.1,
            -0.8, 0.1, -0.8, -0.1, 0, -0.1
        ]);
        gl.bufferData(gl.ARRAY_BUFFER, helicopterRotor, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLES, 0, helicopterRotor.length/2);

        let helicopterRotor2 = new Float32Array([
            -0.1, 0, 0.1, 0, 0.1, 0.8,
            0.1, 0.8, -0.1, 0, -0.1, 0.8,
            -0.1, 0, 0.1, 0, 0.1, -0.8,
            0.1, -0.8, -0.1, -0.8, -0.1, 0
        ]);

        gl.bufferData(gl.ARRAY_BUFFER, helicopterRotor2, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLES, 0, helicopterRotor2.length/2);
    }
}