//Author: Keith Huynh
//Student ID: 44646216

"use strict";

class Helicopter extends GameObject {

    //initialisation
    constructor() {
        super();

        this.position = [0, 0];
        this.rotation = Math.PI;
        this.speed = 5;
        this.rotateSpeed = 3;
    }

    update(deltaTime) {
        check(isNumber(deltaTime));

        //rotate the helicopterBody
        if (Input.leftPressed){
            this.rotation +=  this.rotateSpeed / Math.PI * 2 * deltaTime ;
        }

         if (Input.rightPressed){
            this.rotation -= this.rotateSpeed / Math.PI * 2 * deltaTime;
        }

         if (Input.upPressed){
            //move in the current direction
            this.position[0] +=
                Math.cos(this.rotation) * this.speed * deltaTime;
            this.position[1] +=
                Math.sin(this.rotation) * this.speed * deltaTime;
            this.translation = [this.position[0],this.position[1]];
        }
    }

    //draw helicopter
    renderSelf(gl, colourUniform) {
        check(isContext(gl), isUniformLocation(colourUniform));

        //set the uniforms
        let matrix = Matrix.trs(
            this.position[0], this.position[1],
            this.rotation, 1, 1);

        //set colour brown
        gl.uniform4fv(colourUniform, [0.93, 0.49, 0.19, 1]);

        //draw helicopterBody
        const helicopterBody = new Float32Array([
            0, -0.5, 0, 0.5, 2.5, 0.5,
            2.5, 0.5, 2.5, -0.5, 0, -0.5]);
        gl.bufferData(gl.ARRAY_BUFFER, helicopterBody, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLES, 0, helicopterBody.length/2);

        //draw helicopter sides
        const sides = new Float32Array([
            0.1, -0.5, 0.8, -0.5, 0.1, -0.7,
            0.1, -0.7, 0.8, -0.7, 0.8, -0.5,
            0.1, 0.5, 0.8, 0.5, 0.1, 0.7,
            0.1, 0.7, 0.8, 0.7, 0.8, 0.5
        ]);

        //set colour to darker brown
        gl.uniform4fv(colourUniform, [0.55, 0.35, 0.07, 1]);

        gl.bufferData(gl.ARRAY_BUFFER, sides, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLES, 0, sides.length/2);
    }
}