//Author: Keith Huynh
//Student ID: 44646216

"use strict";

const vertexShaderSource = `
attribute vec4 a_position;
uniform mat3 u_worldMatrix;
uniform mat3 u_viewMatrix;

void main() {
    // convert to homogeneous coordinates 
    vec3 pos = vec3(a_position.xy, 1);

    // multiply by world martix
    pos = u_worldMatrix * pos;

    // multiply by view martix
    pos = u_viewMatrix * pos;

    // output to gl_Position
    gl_Position = vec4(pos.xy,0,1);
}
`;

const fragmentShaderSource = `    
    precision mediump float;
    uniform vec4 u_colour;
    
    void main() {
    gl_FragColor = u_colour; 
}
`;

// Helper functions to compile shaders

function createShader(gl, type, source) {
    check(isContext(gl), isString(source));
    
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        console.error(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}

function createProgram(gl, vertexShader, fragmentShader) {
    check(isContext(gl), isShader(vertexShader, fragmentShader));

    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    const success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        console.error(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
        return null;
    }
    return program;
}

// resize the canvas copied from week 5 game.js code
function resize(canvas) {
    check(isCanvas(canvas));

    const resolution = window.devicePixelRatio || 1.0;

    const displayWidth =
        Math.floor(canvas.clientWidth * resolution);
    const displayHeight =
        Math.floor(canvas.clientHeight * resolution);

    if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        return true;
    }
    else {
        return false;
    }
}

function main() {

    // === Initialisation ===
    const resolution = 50;

    //get the canvas and gl rendering
    const canvas = document.getElementById("c");
    const gl = canvas.getContext("webgl");

    if (gl === null) {
        window.alert("WebGL not supported!");
        return;
    }

    // create GLSL shaders, upload the GLSL source, compile the shaders
    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
    const program =  createProgram(gl, vertexShader, fragmentShader);
    gl.useProgram(program);
    
    // === Per Frame operations ===
    // Initialise the shader attributes & uniforms
    const positionAttribute = gl.getAttribLocation(program, "a_position");
    const worldMatrixUniform = gl.getUniformLocation(program, "u_worldMatrix");
    const viewMatrixUniform =
        gl.getUniformLocation(program, "u_viewMatrix");
    const colourUniform =
        gl.getUniformLocation(program, "u_colour");

    // Initialise the array buffer
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.enableVertexAttribArray(positionAttribute);
    gl.vertexAttribPointer(positionAttribute, 2, gl.FLOAT, false, 0, 0);

    //create new helicopter
    //helicopter body
    const helicopter = new Helicopter();
    helicopter.position = [16, 7];
    helicopter.translation = [16,7];

    //helicopter rotors
    const white = [1, 1, 1, 1];

    //rotor1
    const circle1 = new Circle(white);

    const heli1Focus = new GameObject();
    heli1Focus.parent = helicopter;

    const heli1Pivot = new GameObject();
    heli1Pivot.parent = heli1Focus;


    heli1Pivot.translation = [0.2, 0];

    const rotor1Focus = new GameObject();
    rotor1Focus.parent = heli1Pivot;
    rotor1Focus.rotation = Math.PI/4;

    const heliRotor1 = new HeliRotors();
    heliRotor1.parent = rotor1Focus;
    circle1.parent = heliRotor1;
    //rotor2
    const circle2 = new Circle(white);

    const heli2Focus = new GameObject();
    heli2Focus.parent = helicopter;

    const heli2Pivot = new GameObject();
    heli2Pivot.parent = heli2Focus;


    heli2Pivot.translation = [2.3, 0];

    const rotor2Focus = new GameObject();
    rotor2Focus.parent = heli2Pivot;

    const heliRotor2 = new HeliRotors();
    heliRotor2.parent = rotor2Focus;
    circle2.parent = heliRotor2;

    const camera = new GameObject();
    camera.parent =  helicopter;

    //town and buildings
    const town = new GameObject();

    //helipad building
    const building1 = new Building();
    building1.parent = town;
    building1.translation = [14,8.5];
    building1.scale = 0.7;

    //normal buildings
    const building2 = new Building();
    building2.parent = town;
    building2.translation = [-15,2];
    building2.scale = 1.5;

    const building3 = new Building();
    building3.parent = town;
    building3.translation = [-14,-3.5];
    building3.rotation = -Math.PI/12;
    building3.scale = 1.3;

    const building4 = new Building();
    building4.parent = town;
    building4.translation = [-14,6.5];
    building4.rotation = Math.PI/12;
    building4.scale = 1;

    const building5 = new Building();
    building5.parent = town;
    building5.translation = [-7,6];
    building5.rotation = 4 * Math.PI / 5;
    building5.scale = 0.9;

    //rotor rotate speed
    const heliRotorRotate = 1;

    let update = function(deltaTime) {
       helicopter.update(deltaTime);
       //rotate both rotors
       rotor1Focus.rotation -= Math.PI * 2 * deltaTime / heliRotorRotate;
       rotor2Focus.rotation += Math.PI * 2 * deltaTime / heliRotorRotate;
    };

    let render = function() {
        //clear the screen
        gl.viewport(0, 0, canvas.width, canvas.height);
        //changes colour of background to green
        gl.clearColor(0, 0.69, 0.31, 1);
        gl.clear(gl.COLOR_BUFFER_BIT);

        // scale the view matrix to the canvas size & resolution
        const sx = 2 * resolution / canvas.width;
        const sy = 2 * resolution / canvas.height;

        //change camera view when "c" key is pressed
        if(Input.cPressed === true) {
            // compute inverse world matrix (ignoring scale)
            let viewMatrix = Matrix.identity();

            //
            for (let o = camera; o != null; o = o.parent) {

                viewMatrix = Matrix.multiply(viewMatrix,
                    Matrix.rotation(-o.rotation));
                viewMatrix = Matrix.multiply(viewMatrix,
                    Matrix.rotation(Math.PI / 4));
                viewMatrix = Matrix.multiply(viewMatrix,
                    Matrix.translation(
                        -o.translation[0], -o.translation[1]));
            }
            // calculate the current scale from the matrix
            // by computing the length of the i & j vectors
            const ix = viewMatrix[0];
            const iy = viewMatrix[1];
            const jx = viewMatrix[3];
            const jy = viewMatrix[4];
            const actual_sx = Math.sqrt(ix * ix + iy * iy);
            const actual_sy = Math.sqrt(jx * jx + jy * jy);
            // scale to turn actual scale into desired scale
            const desired_sx = 4 * resolution / canvas.width;
            const desired_sy = 4 * resolution / canvas.height;
            viewMatrix = Matrix.multiply(
                Matrix.scale(desired_sx / actual_sx,
                    desired_sy / actual_sy),
                viewMatrix);
            gl.uniformMatrix3fv(
                viewMatrixUniform, false, viewMatrix);

            gl.uniformMatrix3fv(viewMatrixUniform, false, viewMatrix);

            let matrix = Matrix.identity();

            gl.uniformMatrix3fv(
                worldMatrixUniform, false, matrix);

            //set flooded grey colour
            gl.uniform4fv(colourUniform, [0.69, 0.68, 0.51, 1]);

            //draw river
            const river = new Float32Array([
                -4, 15, 0, -15, 4, 15,
                4, 15, 11, -15, 0, -15]);
            gl.bufferData(gl.ARRAY_BUFFER, river, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, river.length / 2);

            //set colour black
            gl.uniform4fv(colourUniform, [0, 0, 0, 1]);

            //draw padBase
            const padBase = new Float32Array([
                14.1, 7.9, 15.6, 7.9, 14.1, 6.6,
                14.1, 6.6, 15.6, 6.6, 15.6, 7.9]);
            gl.bufferData(gl.ARRAY_BUFFER, padBase, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, padBase.length / 2);

            //set colour white
            gl.uniform4fv(colourUniform, [1, 1, 1, 1]);

            //draw the helipad H
            const heliPad = new Float32Array([
                14.5, 7.6, 14.7, 7.6, 14.5, 6.9,
                14.5, 6.9, 14.7, 7.6, 14.7, 6.9,
                14.7, 7.4, 14.7, 7.1, 15.1, 7.4,
                15.1, 7.4, 15.1, 7.1, 14.7, 7.1,
                15.1, 7.6, 15.3, 7.6, 15.1, 6.9,
                15.1, 6.9, 15.3, 7.6, 15.3, 6.9
            ]);
            gl.bufferData(gl.ARRAY_BUFFER, heliPad, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, heliPad.length / 2);

            town.render(gl, worldMatrixUniform, colourUniform, matrix);
        }

        else {
            let viewMatrix = Matrix.scale(sx, sy);
            gl.uniformMatrix3fv(
                viewMatrixUniform, false, viewMatrix);

            gl.uniformMatrix3fv(viewMatrixUniform, false, viewMatrix);

            let matrix = Matrix.identity();

            gl.uniformMatrix3fv(
                worldMatrixUniform, false, matrix);

            //set flooded grey colour
            gl.uniform4fv(colourUniform, [0.69, 0.68, 0.51, 1]);

            //draw river
            const river = new Float32Array([
                -4, 15, 0, -15, 4, 15,
                4, 15, 11, -15, 0, -15]);
            gl.bufferData(gl.ARRAY_BUFFER, river, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, river.length / 2);

            //set colour black
            gl.uniform4fv(colourUniform, [0, 0, 0, 1]);

            //draw helipad base
            const padBase = new Float32Array([
                14.1, 7.9, 15.6, 7.9, 14.1, 6.6,
                14.1, 6.6, 15.6, 6.6, 15.6, 7.9]);
            gl.bufferData(gl.ARRAY_BUFFER, padBase, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, padBase.length / 2);

            //set colour white
            gl.uniform4fv(colourUniform, [1, 1, 1, 1]);

            //draw the helipad H
            const heliPad = new Float32Array([
                14.5, 7.6, 14.7, 7.6, 14.5, 6.9,
                14.5, 6.9, 14.7, 7.6, 14.7, 6.9,
                14.7, 7.4, 14.7, 7.1, 15.1, 7.4,
                15.1, 7.4, 15.1, 7.1, 14.7, 7.1,
                15.1, 7.6, 15.3, 7.6, 15.1, 6.9,
                15.1, 6.9, 15.3, 7.6, 15.3, 6.9
            ]);
            gl.bufferData(gl.ARRAY_BUFFER, heliPad, gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, heliPad.length / 2);

            town.render(gl, worldMatrixUniform, colourUniform, matrix);
            helicopter.render(gl, worldMatrixUniform, colourUniform, matrix);
        }
    };

    // animation loop
    let oldTime = 0;

    //animate used to resize canvas every frame
    let animate = function(time) {
        time = time / 1000;
        let deltaTime = time - oldTime;
        oldTime = time;

        resize(canvas);
        update(deltaTime);
        render();

        requestAnimationFrame(animate);
    }

    animate(0);
}    

