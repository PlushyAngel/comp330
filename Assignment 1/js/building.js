//Author: Keith Huynh
//Student ID: 44646216

"use strict";

class Building extends GameObject{

    //initialisation
    constructor() {
        super();

        this.position = [0, 0];
        this.rotation = 0;
    }

    //draw town elements
    renderSelf(gl, colourUniform) {
        check(isContext(gl), isUniformLocation(colourUniform));

        //set the uniforms
        let matrix = Matrix.trs(
            this.position[0], this.position[1],
            this.rotation, 1, 1);


        //set colour to colour to dark brown
        gl.uniform4fv(colourUniform, [0.77, 0.35, 0.07, 1]);

        //draw building
        const building = new Float32Array([0, -0.7, 0, 0.7, 2.5, 0.7, 2.5, 0.7, 2.5, -0.7, 0, -0.7]);
        gl.bufferData(gl.ARRAY_BUFFER, building, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLES, 0, building.length/2);

        //set colour to colour to black
        gl.uniform4fv(colourUniform, [0, 0, 0, 1]);

        //draw outlines
        const line = new Float32Array([
            //outline rectangle
            0, -0.7, 0, 0.7,
            0, 0.7, 2.5, 0.7,
            2.5, 0.7, 2.5, -0.7,
            2.5, -0.7, 0, -0.7,

            //outline inside
            0, -0.7,0.8,0,
            0, 0.7,0.8,0,
            0.8, 0, 1.7, 0,
            1.7, 0, 2.5, 0.7,
            1.7, 0, 2.5, -0.7,
        ]);

        gl.bufferData(gl.ARRAY_BUFFER, line, gl.STATIC_DRAW);
        gl.drawArrays(gl.LINES, 0, line.length/2);
    }
}