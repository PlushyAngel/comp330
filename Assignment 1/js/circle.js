//Code taken from week 5 COMP330 code
//Author: Keith Huynh
//Student ID: 44646216

"use strict";

class Circle extends GameObject{


    /**
     * Construct a polygon representing a unit circle with the specified number of sides and colour
     */

    constructor(colour) {
        check(isArray(colour));
        super();

        //number of triangles used to construct circle
        const nSides = 50;

        //change colour to specified colour in game.js
        this.colour = colour;
        this.points = new Float32Array(nSides * 2);
        const theta = 2* Math.PI / nSides;
        //construct array of points for the triangle fan
        for (let i = 0; i < nSides; i++) {
            this.points[2*i] = Math.cos(i * theta)/10;
            this.points[2*i+1] = Math.sin(i * theta)/10;
        }
    }

    renderSelf(gl, colourUniform) {
        check(isContext(gl), isUniformLocation(colourUniform));

        gl.uniform4fv(colourUniform, this.colour);
        gl.bufferData(gl.ARRAY_BUFFER, this.points, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, this.points.length / 2);
    }

}