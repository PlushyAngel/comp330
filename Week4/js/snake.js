class Snake {

	//initialisation
	constructor() {
		this.position = [0,0];
		this.rotation = 0;
		this.speed = 5;

		this.length = 0;
		this.maxLength = 5;
		this.segmentLength = [];
		this.width = 0.5;
		this.body = [0, this.width/2, 0, -this.width/2];
	}

	//
	// snakeon each frame
	update(deltaTime) {
		check(isNumber(deltaTime));

		//rotate the head
		if (Input.leftPressed){
			this.rotation = Math.PI;
		}

		else if (Input.rightPressed){
			this.rotation = 0;
		}

		else if (Input.upPressed){
			this.rotation = Math.PI/2;
		}

		else if (Input.downPressed){
			this.rotation = 3*Math.PI/2	;
		}

		//copy old position
		let oldX = this.position[0];
		let oldY = this.position[1];

		//move in the current direction
		this.position[0] +=
			Math.cos(this.rotation) * this.speed * deltaTime;
		this.position[1] +=
			Math.sin(this.rotation) * this.speed * deltaTime;

		//get tangent vector (normalissed)
		let dx = this.position[0] - oldX;
		let dy = this.position[1] - oldY;
		const dd = Math.sqrt(dx * dx + dy * dy);
		dx /= dd;
		dy /= dd;

		//compute normal vector
		let nx = dy;
		let ny = -dx;

		//add this vector to create points on left and right
		let lx = this.position[0] + nx * this.width / 2
		let ly = this.position[1] + ny * this.width / 2
		let rx = this.position[0] - nx * this.width / 2
		let ry = this.position[1] - ny * this.width / 2

		//add points to body
		this.body.push(lx, ly, rx, ry);

		//add position to body
		//this.body.push(this.position[0], this.position[1]);

		//keep track of length of body
		//(measured in seconds)
		this.segmentLength.push(deltaTime);
		this.length += deltaTime;

		//prune if necessary
		while (this.length > this.maxLength) {
				//remove point from trail
				this.body.shift();
				this.body.shift();
				this.body.shift();
				this.body.shift();

				//decrease length
				this.length -= this.segmentLength.shift();
		}
	}

	//draw snake
	render(gl, worldMatrixUniform, colourUniform) {
		check(isContext(gl), isUniformLocation(worldMatrixUniform, colourUniform));

		//set the uniforms
		let matrix = Matrix.trs(
			this.position[0], this.position[1],
			this.rotation, 1, 1);

		gl.uniformMatrix3fv(
			worldMatrixUniform, false, matrix);

		//set colour to dark green
		gl.uniform4fv(colourUniform, [0, 0.75, 0, 1]);

		//draw head
		const head = new Float32Array([0, -0.5, 0, 0.5, 1, 0]);
		gl.bufferData(gl.ARRAY_BUFFER,
			head, gl.STATIC_DRAW);

		gl.drawArrays(gl.TRIANGLES, 0, head.length/2);

		//set uniforms for body
		gl.uniformMatrix3fv(
			worldMatrixUniform, false, Matrix.identity());
		gl.uniform4fv(colourUniform, [0, 0.75, 0, 1]);

		//draw body
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.body), gl.STATIC_DRAW);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.body.length/2);
	}
}